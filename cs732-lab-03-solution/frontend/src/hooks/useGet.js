import { useState, useEffect } from 'react';
import axios from 'axios';

/**
 * A custom hook which fetches data from the given URL. Includes functionality to determine
 * whether the data is still being loaded or not.
 */
export default function useGet(url, initialState = null) {

    const [data, setData] = useState(initialState);
    const [isLoading, setLoading] = useState(false);

    // For the reFetch() function.
    const [count, setCount] = useState(0);

    // This will cause a re-render...
    function reFetch() {
        setCount(count + 1);
    }

    useEffect(() => {
        async function fetchData() {
            setLoading(true);

            const response = await axios.get(url);
            setData(response.data);
            setLoading(false);

        }
        fetchData();
    }, [url, count]); // When count changes, the fetch will be redone.

    return { data, isLoading, reFetch };
}